import math
import numpy as np
import random
import sys

iteration = 1000
groupsize = 256
child = 128
length = 20
alleles = (0,1)
mutrate = 0.1
target = -959.6407

def Decoding(_chromosome):
  x = 0
  y = 0
  tmp = int(length/2)
  decode = length/2-1
  tmpx = []
  tmpy = []
  for a in range(tmp):
    tmpx.append(_chromosome[a])
  for b in range(tmp, length):
    tmpy.append(_chromosome[b])
  for i in range(int(length/2-1), 0, -1):
    x = x + tmpx[i]*math.pow(2,int(length/2-1-i))
    y = y + tmpy[i]*math.pow(2,int(length/2-1-i))
  if tmpx[0] == 1:
    x = -(math.pow(2,int(length/2)-1)-x)
  if tmpy[0] == 1:
    y = -(math.pow(2,int(length/2)-1)-y)
  return x,y

def FitnessFunc(x,y):
  return -(y+47) * math.sin(math.sqrt(abs(x/2+(y+47)))) - x*math.sin(math.sqrt(abs(x-(y+47))))

def seperate_x(_chromosome, _point):
  tmpx = []
  for i in range(0, _point):
    tmpx.append(_chromosome[i])
  return tmpx

def seperate_y(_chromosome, _point):
  tmpy = []
  for i in range(_point, length):
    tmpy.append(_chromosome[i])
  return tmpy

def crossover(_head,_tail):
  _group = []
  _cromosome = []
  random.shuffle(_head)
  random.shuffle(_tail)
  for i in range(child):
    _chromosome = _head[i] + _tail[i]
    _group.append(_chromosome)
  return _group

def mutation(_group):
  _mut = int(length*mutrate)
  for i in range(child):
    for _ in range(_mut):
      j = random.randint(0,length-1)
      _group[i][j] = not _group[i][j]
  return _group

def main():
  group = []
  childgroup = []
  matepool = []
  chromesome = []
  score = []
  shiftedscore = []
  sortedscore = []
  head = []
  tail = []
  xx,yy = 0,0
  point = random.randint(1,length-1)
  best = 0

  fp = open('train.csv','w',encoding='UTF-8')
  fp.write('x, y, output\n')
# GA init
  for j in range(groupsize):
    chromosome = [bool(random.choice(alleles)) for k in range(length)]
    group.append(chromosome)

# GA loop
  for i in range(iteration):
# Fitnessing
    for l in range(len(group)):
      xx, yy = Decoding(group[l])
      score.append(FitnessFunc(xx,yy))
# Sorting
    for k in range(len(group)):
      shiftedscore.append(abs(score[k] - target))
    sortedscore = sorted(shiftedscore)
# Select chromosome to mating group
    for j in range(child):
      tmp0 = shiftedscore.index(sortedscore[j])
      best = score[tmp0]
      tmp1 = group[tmp0]
      matepool.append(tmp1)
    for m in range(child):
      head.append(seperate_x(matepool[m], point))
      tail.append(seperate_y(matepool[m], point))
    childgroup = crossover(head,tail)
    if mutrate > 0:
      childgroup = mutation(childgroup)

# Find poor adaptability chromosome
    if i != 0:
      for x in range(groupsize+child-1, groupsize-1,-1):
        check = shiftedscore.count(sortedscore[x])
        if group.count(-1) == child:
          pass
        if check > 1:
          for z in range(check-1):
            try:
              tmp2 = shiftedscore.index(sortedscore[x])
              x = x+1
            except:
              pass
            shiftedscore[tmp2] = -9999
            if group.count(-1) == child:
              pass
            else:
              group[tmp2] = -1
        else:
          tmp2 = shiftedscore.index(sortedscore[x])
          if group.count(-1) == child:
            pass
          else:
            group[tmp2] = -1
# Remove poor chromosome
      for _ in range(child):
        group.remove(-1)

# Add childgroup to group
    group = group + childgroup

# temporary storage cleanup
    childgroup = []
    head = []
    tail = []
    score = []
    sortedscore = []
    shiftedscore = []

# save train result
    fp.write('%d, %d, %f\n' %(xx, yy, best))
# print result each 100 iteration
    if i%100 == 0:
      print('Iteration:%d , x=%d, y=%d, score=%f' % (i,xx,yy,best))

  fp.close()


main()
